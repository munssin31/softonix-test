var front = {

    init: function () {
        this.events();
    },


    events: function () {
        //mob menu
        $(".hamb").on('click', function () {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active');
                $('.nav').css('right', '0');
            } else {
                $(this).removeClass('active');
                $('.nav').css('right', '-240px');
            }
        });
        //mob menu

        //mob sub-menu
        $(document).on('click', '.drop-down', function (e) {
            $(this).parent().find('.sub-menu__list').slideToggle();
            $(this).toggleClass('active');
        });
        //mob sub-menu

    }

};

$(function () {
    front.init();
});


console.log("%cSite create by Bohdan Hulovatyi", "color: green; font-size:28px; font-style: italic; text-align: center;");
